CREATE TABLE IF NOT EXISTS "Recs" (
  "start" int4  DEFAULT NULL,
  "end" int4 DEFAULT NULL
);


CREATE TABLE IF NOT EXISTS "Errors" (
  "qualifier" varchar(20) DEFAULT NULL,
  "desc" varchar(1000) DEFAULT NULL,
  "read" boolean DEFAULT NULL,
  "time" int DEFAULT NULL,
  "id" Serial not null,
  CONSTRAINT errors_pk PRIMARY KEY ("id")
);

CREATE TABLE if not exists "Users" (
	"UserName" varchar(20) NULL DEFAULT NULL::character varying,
	"Token" varchar(2000) NULL DEFAULT NULL::character varying,
	"UserPass" varchar(100) NOT NULL,
	"UID" Serial NOT NULL,
	CONSTRAINT users_pk_id PRIMARY KEY ("UID"),
	CONSTRAINT users_un_name UNIQUE ("UserName"),
	CONSTRAINT users_un_token UNIQUE ("Token")
);
