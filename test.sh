#!/bin/bash

trap "docker compose -f ./docker-compose.yml.test down" EXIT

docker compose -f ./docker-compose.yml build piwatch-client
docker compose -f ./docker-compose.yml up database piwatch-client piwatch-api
