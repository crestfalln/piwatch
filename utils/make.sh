#!/bin/bash

function build {
  cd website/api
  yarn install 
  yarn build
  cd ..
  cd client
  yarn install
  yarn build
  cd ../..
}

function make_tarball {
  tmp=piwatch-${tag}
  mkdir -p releases
  mkdir -p ${tmp}
  install -o root -g root -m 640 -D ./website/api/package.json -t ${tmp}/website/api/
  install -o root -g root -m 640 -D ./website/api/Dockerfile -t ${tmp}/website/api/
  install -o root -g root -m 640 -D ./website/api/bundle/* -t ${tmp}/website/api/bundle/
  install -o root -g root -m 640 -D ./website/client/Dockerfile -t ${tmp}/website/client/
  install -o root -g root -m 644 -D ./website/client/nginx.conf -t ${tmp}/website/client/
  fd . ./script/src/ -tf --exclude=PiWatch.py -x install -o root -g root -m 640 -D {} -t ${tmp}/script/
  install -o root -g root -m 640 -D ./script/requirements.txt -t ${tmp}/script/
  install -o root -g root -m 751  ./script/src/PiWatch.py -t ${tmp}/script/
  install -o root -g root -m 644 -D ./environment/script.env -t ${tmp}/script/
  install -o root -g root -m 644 -D ./environment/website.env -t ${tmp}/
  fd -tf -I . ./website/client/build/ -x install -o root -g root -m 640 -D {} -t ${tmp}/{//}/
  fd -tf -I . ./database -x install -o root -g root -m 644 -D {} -t ${tmp}/{//}/
  install -o root -g root -m 751  ./utils/init.sh -t ${tmp}/
  install -o root -g root -m 640 ./docker-compose.yml -t ${tmp}/
  tar -czf releases/${tmp}.tar.gz  \
      --transform="flags=r;s|${tmp}/website.env|${tmp}/.env|" \
      --transform="flags=r;s|${tmp}/script/script.env|${tmp}/script/.env|" \
      ${tmp}
  rm -rf ${tmp}

}

if ! [ "$1" = "--skip-build" ]
then
  build
fi

tag=$(git describe HEAD)
if [ "$tag" = "" ]
then
  tag=$(git rev-parse HEAD | head -c 8 )
fi

make_tarball
exit
